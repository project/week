<?php

use Drupal\Component\Utility\Html;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_tokens().
 */
function week_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'week' && !empty($data['week'])) {
    $week_value = $data['week'];

    list ($year, $week) = explode('-', $week_value);
    $week_date = DrupalDateTime::createFromArray(['year' => (int) $year, 'month' => date('n'), 'day' => 1, 'second' => 0, 'minute' => 0, 'hour' => 0]);
    $week_date->setISODate($year, $week);

    foreach ($tokens as $name => $original) {
      list ($day, $format) = explode(':', $name);

      if (!isset($day) || !isset($format)) {
        continue;
      }

      switch ($day) {
        case 'monday':
          $monday = clone $week_date;
          $monday->modify('monday this week');

          $replacements[$original] = $monday->format($format);
          break;

        case 'sunday':
          $sunday = clone $week_date;
          $sunday->modify('sunday this week');
          $replacements[$original] = $sunday->format($format);
          break;
      }
    }
  }

  return $replacements;
}
