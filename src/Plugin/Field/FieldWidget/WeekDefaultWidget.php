<?php

namespace Drupal\week\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'week' field widget.
 *
 * @FieldWidget(
 *   id = "week_default",
 *   label = @Translation("Week Default"),
 *   field_types = {"week"},
 * )
 */
class WeekDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'year_start' => '-3',
      'year_end' => '+3',
      'week_format' => '[week:monday:M jS] - [week:sunday:M jS]',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // TODO: Add more options for showing abbrevations for months.

    $element['year_start'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Year start'),
      '#default_value' => $this->getSetting('year_start'),
      '#description' => $this->t('Enter a relative year like "-3", or a static start year like "2017"'),
    ];

    $element['year_end'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Year end'),
      '#default_value' => $this->getSetting('year_end'),
      '#description' => $this->t('Enter a relative year like "+3", or a static end year like "2022"'),
    ];

    $element['week_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Week format'),
      '#default_value' => $this->getSetting('week_format'),
      '#description' => $this->t('Available tokens are [week:sunday:format] and [week:monday:format] where format is anything passed to DrupalDateTime::format().'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Format: @format', ['@format' => $this->getSetting('week_format')]);
    return $summary;
  }

  private function getYearOptions() {
    $setting_year_start = $this->getSetting('year_start');
    $year_start = $this->year($setting_year_start);
    $setting_year_end = $this->getSetting('year_end');
    $year_end = $this->year($setting_year_end);
    return ['' => $this->t('- Select -')] + DateHelper::years($year_start, $year_end, TRUE);
  }

  private function getMonthOptions() {
    return ['' => $this->t('- Select -')] + DateHelper::monthNames(TRUE);
  }

  private function year($str) {
    $str = trim($str);
    if (strpos($str, '+') !== FALSE) {
      $int = (int) trim(str_replace('+', '', $str));
      return date('Y') + $int;
    }
    if (strpos($str, '-') !== FALSE) {
      $int = (int) trim(str_replace('-', '', $str));
      return date('Y') - $int;
    }
    return (int) $str;
  }

  private function getAjaxSettings($wrapper_id) {
    return [
      'callback' => [static::class, 'updateWidget'],
      'wrapper' => $wrapper_id,
      'progress' => [
        'type' => 'throbber',
      ],
    ];
  }

  private function getWeekOptions($year, $month) {
    // Get day 1 of the current month.
    $start = DrupalDateTime::createFromArray(['year' => $year, 'month' => $month, 'day' => 1, 'second' => 0, 'minute' => 0, 'hour' => 0]);

    // If the first day of the month isn't a monday, (very likely), then convert to that monday.
    $start->modify('monday this week');
    // If that monday is in the previous month, we won't include that as
    // we'll call that a week that starts in the previous month.
    $start_month = $start->format('n');
    if (($start_month < $month) || (($start_month === "12") && ($month === "1"))) {
      // Move to next monday.
      $start->modify('next monday');
    }

    $options = ['' => $this->t('- Select -')];
    while ($start->format('n') === $month) {
      $sunday = clone $start;
      $sunday->modify('sunday this week');
      $options["{$start->format('Y')}-{$start->format('W')}"]  = "{$start->format('M jS')} - {$sunday->format('M jS')}";
      $start->add(new \DateInterval('P1W'));
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    $parents = $form['#parents'];
    // Create an ID suffix from the parents to make sure each widget is unique.
    $id_suffix = $parents ? '-' . implode('-', $parents) : '';
    $wrapper_id = $field_name . '-week-widget-wrapper' . $id_suffix . "-{$delta}";


    $value = isset($items[$delta]->value) ? $items[$delta]->value : NULL;
    list ($year, $week) = $value ? explode('-', $value) : [date('Y'), date('W')];

    $month_date = DrupalDateTime::createFromArray(['year' => (int) $year, 'month' => date('n'), 'day' => 1, 'second' => 0, 'minute' => 0, 'hour' => 0]);
    $month_date->setISODate($year, $week);
    $month = $month_date->format('n');

    if ($value = $form_state->getValue([$field_name, $delta, 'week'])) {
      $year = $value['year'];
      $month = $value['month'];
    }

    $element['week'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $wrapper_id,
        'class' => ['week-default-widget'],
      ],
      'year' => [
        '#type' => 'select',
        '#title' => $this->t('Year'),
        '#options' => $this->getYearOptions(),
        '#default_value' => $year,
        '#ajax' => $this->getAjaxSettings($wrapper_id),
      ],
      'month' => [
        '#title' => $this->t('Month'),
        '#type' => 'select',
        '#options' => $this->getMonthOptions(),
        '#default_value' => $month,
        '#ajax' => $this->getAjaxSettings($wrapper_id),
      ],
      'value' => [
        '#title' => $this->t('Week'),
        '#type' => 'select',
        '#options' => $this->getWeekOptions($year, $month),
        '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      ],
    ];

    return $element;
  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $new_values = [];
    foreach ($values as $delta => $value) {
      $new_values[$delta] = $value['week']['value'] ?? NULL;
    }
    return $new_values;
  }

  public static function updateWidget(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $wrapper_id = $triggering_element['#ajax']['wrapper'];

    $parents = array_slice($triggering_element['#array_parents'], 0, -1);
    $element = NestedArray::getValue($form, $parents);

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand("#$wrapper_id", $element));

    return $response;
  }

}
