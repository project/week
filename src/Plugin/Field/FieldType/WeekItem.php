<?php

namespace Drupal\week\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\week\DateTimeComputed;

/**
 * Defines the 'week' field type.
 *
 * @FieldType(
 *   id = "week",
 *   label = @Translation("Week"),
 *   category = @Translation("General"),
 *   default_widget = "week_default",
 *   default_formatter = "week_default"
 * )
 *
 * NOTE: select * from node__field_week_test where CONVERT(REPLACE(field_week_test_value, '-', ''), SIGNED) > 202105
 */
class WeekItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Text value'))
      ->setRequired(TRUE);

    $properties['monday'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Computed monday date'))
      ->setDescription(t('The computed monday DateTime object.'))
      ->setComputed(TRUE)
      ->setClass(DateTimeComputed::class)
      ->setSetting('day', 'monday');

    $properties['sunday'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Computed sunday date'))
      ->setDescription(t('The computed sunday DateTime object.'))
      ->setComputed(TRUE)
      ->setClass(DateTimeComputed::class)
      ->setSetting('day', 'sunday');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'value' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Column description.',
        'length' => 8,
      ],
    ];

    $schema = [
      'columns' => $columns,
    ];

    return $schema;
  }

  /**
   * @param \DateTime|\Drupal\Core\Datetime\DrupalDateTime $datetime
   *   Either a PHP DateTime or DrupalDateTime which have ::format().
   * @return string
   */
  public static function formatDateForStorage($datetime) {
    $year = $datetime->format('Y');
    $week = str_pad($datetime->format('W'), 2, '0', STR_PAD_LEFT);
    return $year . '-' . $week;
  }

  public static function randomValue() {
    $year = date('Y');
    $week = str_pad(mt_rand(1, 52), 2, '0', STR_PAD_LEFT);
    return mt_rand($year - 20, $year + 20) . '-' . $week;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = static::randomValue();
    return $values;
  }

}
