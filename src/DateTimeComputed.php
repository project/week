<?php

namespace Drupal\week;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedData;
use Drupal\Core\TypedData\TypedDataInterface;

class DateTimeComputed extends TypedData {

  /**
   * Cached computed date.
   *
   * @var \DateTime|null
   */
  protected $date = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    if (!$definition->getSetting('day')) {
      throw new \InvalidArgumentException("The definition's 'day' key has to specify the day in the week to be computed.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if ($this->date !== NULL) {
      return $this->date;
    }

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    $item = $this->getParent();
    if (!$value = $item->value) {
      return NULL;
    }

    try {
      list ($year, $week) = explode('-', $value);
      $this->date = DrupalDateTime::createFromArray(['year' => (int) $year, 'month' => date('n'), 'day' => 1, 'second' => 0, 'minute' => 0, 'hour' => 0]);
      $this->date->setISODate($year, $week);
      $this->date->modify("{$this->definition->getSetting('day')} this week");
    }
    catch (\Exception $e) {
      // @todo Handle this.
    }

    return $this->date;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    $this->date = $value;
    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->name);
    }
  }

}
