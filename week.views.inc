<?php

/**
 * Implements hook_field_views_data().
 */
function week_field_views_data(\Drupal\field\FieldStorageConfigInterface $field_storage) {
  $data = views_field_default_views_data($field_storage);
  $column_name = $field_storage->getMainPropertyName();

  foreach ($data as $table_name => $table_data) {
    $data[$table_name][$field_storage->getName() . '_' . $column_name]['filter']['id'] = 'week';
  }
  return $data;
}
